# Hangman API
A RESTful API to play hangman.

<br/>

## Online demo
https://hangman-ib.herokuapp.com/api/

<br/>

## The game object
    {
        "id": 1,
        "lives": 6,
        "status": 0,
        "secret_word": "POOP",
        "guessed_word": "P__P",
        "guessed_letters": {
            "A": false,
            "B": false,
            "C": false,
            "D": false,
            "E": false,
            "F": false,
            "G": false,
            "H": false,
            "I": false,
            "J": false,
            "K": false,
            "L": false,
            "M": false,
            "N": false,
            "O": false,
            "P": true,
            "Q": false,
            "R": false,
            "S": false,
            "T": false,
            "U": false,
            "V": false,
            "W": false,
            "X": false,
            "Y": false,
            "Z": false
        },
        "created_at": "2021-09-15T16:55:09.000000Z",
        "updated_at": "2021-09-15T16:56:11.000000Z"
    },

### Fields:
| Name | Type | Description |
|---|---|---|
| `id`  | integer | The unique id of the game. |
| `lives` | integer | The number of remaining incorrect guesses that can be made before the player loses the game. Initialized at `6` for a new game. |
| `status` | integer | The status of the game. `0` is ongoing -- there are still remaining lives and the word has not been completely guessed. `1` is won -- the word has been completely guessed. `-1` is lost -- there are no more remaining lives and the word has not been completely guessed. Initialized at `0` for a new game. |
| `secret_word` | string | The word being guessed. This will only appear in the game object when the game status is either `1` (won) or `-1` (lost). For ongoing games with status `0`, the `secret_word` will be hidden. |
| `guessed_word` | string | The current state of the word being guessed. A letter indicates a match with the `secret_word` on the same position. An underscore `_` indicates that the letter in that position has not yet been guessed. |
| `guessed_letters` | object | An object with letter keys from A to Z, with boolean values indicating whether the letter has already been guessed. The values are initialized to `false` for a new game. When a letter guess is made, regardless if found in the `secret_word`, the value of the letter will be set to `true`. |
| `created_at` | timestamp | Timestamp when the game was created. |
| `updated_at` | timestamp | Timestamp when the game was last updated. |
<br/>

## List all the games
`GET /api/games`

Returns a list of all the game objects.


<br/>

## Get a game by id
`GET /api/games/{id}`

Returns the game object with the specified id.


<br/>

## Create a new game
`POST /api/games`

Returns the created game object.

### Parameters:

| Name  | Type   | Description   |
|---|---|---|
| `secret_word` (Optional)  | string | The word to be guessed. A single word containing only letters A to Z, without any numbers or special characters. If not specified, `secret_word` will be set to a random word from the English dictionary with the same rules.   |

<br/>

## Guess a letter for a game
`POST /api/games/{id}`

Sends a guess to the game with the specified id. Returns the updated game object for a valid guess in an ongoing game (status `0`). Returns the game object without any changes if the game is either won or lost (status `1` or `-1`).


### Parameters:
| Name  | Type   | Description   |
|---|---|---|
| `guess` (Required)  | string | The guessed letter. Any letter from A to Z. |

<br/>

## Delete a game
`DELETE /api/games/{id}`

Deletes the game with the specified id. Returns 1 if the game with the specified id is successfully deleted.

