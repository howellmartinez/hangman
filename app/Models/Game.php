<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['secret_word'];
    protected $casts = [
        'guessed_letters' => 'array'
    ];
    use HasFactory;
}
