<?php

use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// List games
Route::get('games', function () {
    return Game::all();
});

// Get a game by id
Route::get('games/{id}', function ($id) {
    $game = Game::findOrFail($id);
    // Show secret_word if not ongoing
    if ($game->status !== 0) {
        $game->makeVisible('secret_word');
    }
    return $game;
});

// Create a new game
Route::post('games', function (Request $request) {
    $secretWord = "";

    $validator = Validator::make($request->all(), [
        'secret_word' => 'sometimes|alpha',
    ]);

    if ($validator->fails()) {
        return $validator->errors()->toJson();
    }

    if (request()->has('secret_word')) {
        $secretWord = strtoupper(request()->input('secret_word'));
    } else {
        $file = file(storage_path('app/words_alpha.txt'));
        $randomWord = $file[rand(0, count($file) - 1)];
        $secretWord = strtoupper(substr($randomWord, 0, -2));
    }
    $guessedWord = str_repeat('_', strlen($secretWord));
    return Game::create([
        'lives' => 6,
        'status' => 0,
        'secret_word' => $secretWord,
        'guessed_word' => $guessedWord,
        'guessed_letters' => array_map(fn () => false, array_flip(range('A', 'Z'))),
    ]);
});

Route::post('games/{id}', function (Request $request, $id) {
    $game = Game::findOrFail($id);

    // Return the game with the secret_word if not ongoing
    if ($game->status !== 0) {
        return $game->makeVisible('secret_word');
    }

    $validator = Validator::make($request->all(), [
        'guess' => 'required|alpha|max:1',
    ]);

    if ($validator->fails()) {
        return $validator->errors()->toJson();
    }

    $guess = strtoupper($request->input('guess'));
    $guessedLetters = $game->guessed_letters;

    // Letter has already been guessed
    if ($guessedLetters[$guess] === true) {
        return $game;
    } else {
        $guessedLetters[$guess] = true;
    }

    $secretWord = strtoupper($game->secret_word);
    $guessedWord = $game->guessed_word;
    $offset = 0;

    while (strpos($secretWord, $guess, $offset) !== false && $offset <= strlen($secretWord)) {
        $index = strpos($secretWord, $guess, $offset);
        $guessedWord = substr_replace($guessedWord, $guess, $index, 1);
        $offset = $index + 1;
    }

    // Nothing was guessed
    if ($guessedWord === $game->guessed_word) {
        $game->lives = $game->lives - 1;
    } else {
        $game->guessed_word = $guessedWord;
    }

    $game->guessed_letters = $guessedLetters;

    // Win the game if word is guessed
    if (strtoupper($game->secret_word) === strtoupper($game->guessed_word)) {
        $game->status = 1;
    }
    // Lose the game if word not guessed and no more lives
    elseif ($game->lives <= 0) {
        $game->status = -1;
    }

    $game->save();
    return $game;
});

// Delete a game by id
Route::delete('games/{id}', function ($id) {
    return Game::findOrFail($id)->delete();
});
